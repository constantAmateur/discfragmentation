# Readme

This repository contains the code needed to run the simulations presented in Young & Clarke [(2015a)](http://arxiv.org/abs/1506.02560) and [(2015b)](http://arxiv.org/abs/1510.05290).  The simulations were performed using a version of GADGET that can be found in [this repository](https://github.com/constantAmateur/gadget2-AccretionDiscs) and FARGO which can be found [here](http://fargo.in2p3.fr/-Legacy-archive-).

Included in this repository are example compilation flags (MAKEFILE), initial conditions generation scripts and config files to be used with these configuration codes.  It should be fairly obvious how to modify these, at least for other simple disc parameters.

# Creating initial conditions

For the SPH simulations, the initial conditions are created using the supplied R script followed by a [C program](https://bitbucket.org/constantAmateur/easyic) to convert the ascii output of the R script to the binary format required by GADGET.  This is a very clunky way of doing things and the far more sensible approach would be to write the initial conditions in binary format directly from R.  This script has largely been included for completeness.  A couple of example initial conditions files have been included in GADGET binary format.

The initial condition generation for FARGO is performed by FARGO itself, all that is needed is to supply the (modified to your needs) version of the supplied config file.

# Post processing

The python code used to perform the post-processing in Young & Clarke (2015b) is included in the file postProcessing.py.  See the paper and the comments in this file for further details of how the analysis was performed.

# Contact

Questions or comments should be sent to my304 [at] ast.cam.ac.uk