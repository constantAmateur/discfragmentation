######################## START PASTE FROM PLAYING.PY ##################################
#Common things that are useful
#Assumed to be run with ipython --pylab
import os,sys
from scipy.interpolate import griddata
from scipy.stats import gaussian_kde
from mypy.data import read_gadget_binary,write_gadget_binary
from mypy.math import match,table
g=5/3.
def saveplot(subdir=None,path=None,name=None,extension='png',history=[]):
  '''
  Saves the current active plot to a file named by taking the plot
  title and doing some sanitising.  Also saves a file with the
  current working directory and the command history with the same name
  and txt extension.  Will over-write both by default.

  If no path is given, default to the last used one (or ~/Plots/ if none).
  If no subdirectory is given, default to the last used one (or top level if none)
  Never pass history...
  '''
  #Work out directory
  if path is None:
    if len(history)==0:
      path=os.path.expanduser("~/Plots/")
    else:
      path=history[-1][0]
  #Work out subdir
  if subdir is None:
    if len(history)==0:
      subdir=''
    else:
      subdir=history[-1][1]
  #Create full path and history element
  fpath=os.path.join(path,subdir)
  histel=[path,subdir]
  #Work out a reasonable automatic name if none given
  if name is None:
    name=plt.gca().get_title().replace(' ','_')+'.'+extension
  #Give it a name if the current one is garbage
  if len(name)==0 or name=='.'+extension:
    name='Random_plot.%s'%(extension)
  #Does it already exist?  Let's add some numbers
  pnum=1
  while os.path.isfile(os.path.join(fpath,name)) or os.path.isfile(os.path.join(fpath,name,'.txt')):
    #Append _number_cnt and try again
    name=name[:name.rfind('.')]+"_number_%d"%pnum+name[name.rfind('.'):]
    pnum=pnum+1
  #Save the figure
  plt.savefig(os.path.join(fpath,name))
  #Save command history
  get_ipython().magic("%history -t -f "+os.path.join(fpath,name)+".txt")
  #Append this plot to the history
  history.append(histel)

def quickplot(pos,what,res=1024,xlims=None,ylims=None,log=False,**kw):
  '''
  Makes a quick interpolation of some SPH data to a grid and plots it.
  '''
  if xlims is None:
    xlims=[min(pos[:,0]),max(pos[:,0])]
  if ylims is None:
    ylims=[min(pos[:,1]),max(pos[:,1])]
  xx=np.linspace(xlims[0],xlims[1],res)
  yy=np.linspace(ylims[0],ylims[1],res)
  xx,yy=np.meshgrid(xx,yy)
  if log:
    what=np.log10(what)
  tmp=griddata(pos[:,:2],what,np.c_[xx.flatten(),yy.flatten()])
  #Mask out any nans
  tmp[np.logical_not(np.isfinite(tmp))]=np.mean(what)
  plt.figure()
  plt.pcolormesh(xx,yy,tmp.reshape(xx.shape),**kw)
  plt.xlim(xlims)
  plt.ylim(ylims)
  plt.gca().axis('equal')
  plt.colorbar()

def rolling_mean(dat,n=.001):
  '''
  Returns a moving average of data in bins of size n.
  If n is less than 1 it is taken to be a fraction of the length of dat.
  '''
  if n<1.0:
    n=len(dat)*n
  n=int(n)
  return np.convolve(dat,np.ones(n)/float(n),'same')

def rolling_std(dat,n=.001):
  '''
  Returns a moving standard deviation of data in bins of size n.
  If n is less than 1 it is taken to be a fraction of the length of dat.
  '''
  if n<1.0:
    n=len(dat)*n
  n=int(n)
  window=np.ones(n)/float(n)
  return np.sqrt(np.convolve(dat*dat,window,'same')-np.convolve(dat,window,'same')**2)

def rolling_window(a, window):
  shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
  strides = a.strides + (a.strides[-1],)
  return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)


class GadgetBinary(object):
  '''
  A binary snapshot that converts to centre of mass/momentum frame
  and calculates cylindrical coordinates, and some other stuff.
  '''
  #Use this many neighbours when calculated azimuthal averages
  track_nngbs=10000
  def __init__(self,fnom,azavg=False):
    '''
    fnom is the name of the file.
    '''
    self.fnom=fnom
    print 'Loading binary snapshot %s'%fnom
    _,self.masses,pos,vel,self.rho,self.hsml,self.uint,self.ids,header,self.extras = read_gadget_binary(fnom,extras=True)
    print 'File loaded, converting coordinates...'
    #Correct hsml
    self.hsml = self.hsml *0.5
    #Translate to centre of mass frame
    Mtot=np.sum(self.masses)
    mmat=np.column_stack((self.masses,self.masses,self.masses))
    self.Mstar=max(self.masses)
    self.Mdisc=Mtot-self.Mstar
    self.q=self.Mdisc/self.Mstar
    self.com = np.sum(mmat*pos,axis=0)/Mtot
    self.vcom = np.sum(mmat*vel,axis=0)/Mtot
    pos = pos - self.com
    vel = vel - self.vcom
    #Calculate cylindrical coordinates
    self.R= np.sqrt(np.sum(pos**2,1))
    self.phi=np.arctan2(pos[:,1],pos[:,0])
    self.vR=(pos[:,0]*vel[:,0]+pos[:,1]*vel[:,1])/self.R
    self.vphi=(-pos[:,1]*vel[:,0]+pos[:,0]*vel[:,1])/self.R
    #Only store 3D things if we're in 3D
    self.D3=False
    if np.any(pos[:,2]!=0):
      self.z=pos[:,2]
      self.vz=vel[:,2]
      self.D3=True
    ##Save the star separately
    #self.pstar=pos[o,]
    #self.vstar=vel[o,]
    #self.idstar=self.ids[o]
    #Drop the star
    o=np.arange(len(self.masses))!=np.argmax(self.masses)
    self.N=len(o)-1
    self.masses=self.masses[o]
    self.R=self.R[o]
    self.phi=self.phi[o]
    self.vR=self.vR[o]
    self.vphi=self.vphi[o]
    if self.D3:
      self.z=self.z[o]
      self.vz=self.vz[o]
    self.ids=self.ids[o]
    #Any other quantity that includes the star needs to be processed here...
    for i in xrange(len(self.extras)):
      sh=self.extras[i].shape
      if max(sh)==self.N+1:
        if len(sh)==1:
          self.extras[i]=self.extras[i][o]
        else:
          self.extras[i]=self.extras[i][o,]
    #Sort everything by R
    o=np.argsort(self.R)
    self.masses=self.masses[o]
    self.R=self.R[o]
    self.phi=self.phi[o]
    self.vR=self.vR[o]
    self.vphi=self.vphi[o]
    if self.D3:
      self.z=self.z[o]
      self.vz=self.vz[o]
    self.rho=self.rho[o]
    self.hsml=self.hsml[o]
    self.uint=self.uint[o]
    self.ids=self.ids[o]
    for i in xrange(len(self.extras)):
      sh=self.extras[i].shape
      if len(sh)==1:
        self.extras[i]=self.extras[i][o]
      else:
        self.extras[i]=self.extras[i][o,]
    #Extra quantities that are easily derived
    self.cs=np.sqrt(g*(g-1.0)*self.uint)
    self.K=self.uint*(g-1.0)/self.rho**(g-1.0)
    if azavg:
      self.azimuthal_average()

  def azimuthal_average(self):
    print 'Calculating averaged quantities'
    #Make nngbs symmetric
    nngbs = self.track_nngbs+self.track_nngbs%2
    offset = nngbs/2
    nngbs = nngbs+1
    window=np.ones(nngbs)/float(nngbs)
    #Take an azimuthal average of H=cs/omega in 2D, get std. deviation of z in 3D
    #Assume G=1.0
    x=self.z if self.D3 else np.sqrt(g*(g-1)*self.uint*self.R**3/self.Mstar)
    #Calculate rolling distribution
    means=np.convolve(x,window,'same')
    sds=np.sqrt(np.convolve(x*x,window,'same')-means**2)
    #Calculate the surface density
    self.sigma=np.empty(self.N)
    self.sigma[offset:-offset] = (self.masses[0]*nngbs)/(np.pi*(self.R[nngbs-1:]**2-self.R[:-nngbs+1]**2))
    #Fill in edges
    self.sigma[-offset-1:]=self.sigma[-offset-1]
    self.sigma[:offset]=self.sigma[offset]
    #If it's 2D, store azimuthal average, otherwise get directly
    self.H=sds if self.D3 else means
    #Estimate the averaged Q from H and Sigma
    #self.Q=(self.H*self.Mstar)/(np.pi*self.sigma*self.R**3)
    #Estimate it using a convolution of cs*omega
    self.Q=np.convolve(self.cs*np.sqrt(self.Mstar/self.R**3),window,'same')/(np.pi*self.sigma)

def gradlims():
  '''
  Calculate the gradient from the plot limits.
  '''
  ys=plt.ylim()
  xs=plt.xlim()
  return (ys[1]-ys[0])/(xs[1]-xs[0])

def ncolour():
  '''
  A generator that will return the next colour in the list
  '''
  num = 0
  colours=['k','b','g','r','m','c','y']
  while True:
    yield colours[num%len(colours)]
    num += 1

def nlstyle():
  '''
  A generator that will return the next line style from the list
  '''
  num = 0
  styles=['-','--','-.',':']
  while True:
    yield styles[num%len(styles)]
    num += 1

def plotpdf(data,res=1000,ax=None,**kw):
  density=gaussian_kde(data)
  xs=np.linspace(np.min(data),np.max(data),res)
  offsets=density(xs)
  if ax is not None:
    ax.plot(xs,offsets,**kw)
  else:
    plt.plot(xs,offsets,**kw)
  return (xs,offsets)


def distbox(data,res=1000,wiggle=.05,mid=1.0,width=.08):
  density=gaussian_kde(data)
  buf=(np.max(data)-np.min(data))*wiggle
  xs=np.linspace(np.min(data)-buf,np.max(data)+buf,res)
  offsets=density(xs)
  dscale=width/np.max(offsets)
  offsets=offsets*dscale
  plt.plot(mid+offsets,xs,'b')
  plt.plot(mid-offsets,xs,'b')
  #Fill it in
  rects=[]
  tmp=np.log10(offsets)
  tmp=tmp-np.min(tmp)
  tmp=tmp/np.max(tmp)
  cols=cm(tmp)
  for i in xrange(len(xs)-1):
    rects.append(mpl.patches.Rectangle((mid-offsets[i],xs[i]),offsets[i]*2,xs[i+1]-xs[i],color=cols[i]))
  ax=plt.gca()
  for p in rects:
    ax.add_patch(p)
  #Plonk on the median in red
  xm=np.median(data)
  off=density(xm)[0]*dscale
  plt.plot([mid-off,mid+off],[xm]*2,'r-')
  #Plonk on markers of the last data-points
  plt.plot([mid-width,mid+width],[np.min(data)]*2,'k')
  plt.plot([mid-width,mid+width],[np.max(data)]*2,'k')

######################## STOP PASTE FROM PLAYING.PY ##################################

import numpy as np
import cPickle
import re
from matplotlib import pyplot as plt
mpl.rcParams.update({'font.size':30})
#These probably won't be used most of the time, but it doesn't hurt to make sure they're set just in case
snap_max=np.inf
include_min=128
follow_max=200000
rdrift=100
rlims=[2.,3.]
big_data=[]
k_perc_cut=50
s_perc_cut=50
cutoffs=[64,128,256,512,1024]
base='/home/my304/output/Archive/Fragmentation/2DSPH/FragPaperData/'

#This only needs to be run once (or when you change the definitions of things for wait times)
for tgt in ['2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_3.0_N_25e4','2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_3.0_N_1e6','2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_3.0_N_4e6','2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_10.0_N_1e6']:
  #Move to target directory
  #os.chdir('/home/myoung/cosmos/scratch/Output/FragBoundary/SPH/')
  os.chdir(base)
  os.chdir(tgt)
  #Get list of snapshots
  sims = [(int(x[-4:]),x) for x in os.listdir('.') if x[:8]=='snapshot']
  sims.sort(key=lambda x:x[0])
  #Initialise data storage
  data=[]
  following={}
  snap_no=200
  #Loop through snapshots
  while snap_no<=min(sims[-1][0],snap_max):
    #Load the snapshot
    print "Loading snapshot_%04d with %d currently being tracked, with completed data for %d"%(snap_no,len(following),len(data))
    fnom='snapshot_%04d'%snap_no
    #fnom='SpiralDisc_%04d'%snap_no
    #extras: 0=alpha,1=sigma,2=rho,3=entropy,4=cs,5=uint,6=vel,7=alpha
    gb=GadgetBinary(fnom)
    #Limit to just the radii that are robust
    o=np.where((gb.R>rlims[0]) & (gb.R<rlims[1]))[0]
    #Find out where the ones being followed are now
    fids=np.array(following.keys())
    m=match(fids,gb.ids)
    #If the particle has walked off the reservation, discard it
    #Throw out the accreted ones
    dead=fids[np.isnan(m)]
    fids=fids[np.isfinite(m)]
    m=m[np.isfinite(m)].astype(int)
    #Throw out those not in our limited R range any more
    oo=np.in1d(m,o)
    dead = np.r_[dead,fids[np.logical_not(oo)]]
    fids=fids[oo]
    m=m[oo]
    #Throw out those that move too much in R
    originalR = np.array([following[x][1][0][0] for x in fids])
    oo=np.abs(gb.R[m]-originalR)<rdrift*originalR
    dead = np.r_[dead,fids[np.logical_not(oo)]]
    fids=fids[oo]
    m=m[oo]
    #Those that are left are in need of updating
    #Get the bits of data we need to update them
    tmp=np.c_[gb.R[m],gb.phi[m],gb.rho[m],gb.vR[m],gb.vphi[m],gb.cs[m],gb.K[m]]
    #Update them one at a time
    for i in xrange(len(fids)):
      following[fids[i]][1].append(tmp[i,])
    lls=[len(following[x][1]) for x in fids]+[0]
    print 'Longest tracked particle, tracked for %d has %d friends'%(max(lls),np.sum(lls==max(lls)))
    #Remove the finished ones, storing or discarding as needed
    for fid in set(dead):
      tmp=following.pop(fid)
      #Don't want super short lived particles
      if len(tmp[1])>include_min:
        data.append([fid,tmp[0],snap_no,tmp[1]])
    #Pick ones that are suitable candidates and we're not already tracking
    #Add the new candidates in
    for m in o[np.logical_not(np.in1d(gb.ids[o],following.keys()))][:(follow_max-len(following))]:
      following[gb.ids[m]]=[snap_no,[np.array(
        [gb.R[m],gb.phi[m],gb.rho[m],gb.vR[m],gb.vphi[m],gb.cs[m],gb.K[m]]
        )]]
    snap_no = snap_no+1
  #Now finalise things
  for fid in following.keys():
    tmp=following[fid]
    data.append([fid,tmp[0],snap_no,tmp[1]])
  cPickle.dump(data,open('data.pickle','wb'))
  #np.savez('data.npz',data=data)
  #Use a restricted subset of data
  rdata=[x[:-1]+[x[-1][-include_min:]] for x in data if len(x[-1])>include_min]
  #Get all the jumps
  jumps=[[] for i in xrange(len(rdata))]
  for i in xrange(len(rdata)):
    print i
    #Find all the jumps in entropy
    #0=R,1=phi,2=rho,3=vR,4=vphi,5=cs,6=K
    dd=np.array(rdata[i][-1])
    tt=np.cumsum(1./dd[:,0]**1.5)
    vcart=np.c_[np.cos(dd[:,1])*dd[:,3]-np.sin(dd[:,1])*dd[:,4],np.sin(dd[:,1])*dd[:,3]+np.cos(dd[:,1])*dd[:,4]]
    #Find the locations of shocks
    ictr=0
    while ictr<len(tt)-1:
      if dd[ictr+1,6]>dd[ictr,6]:# and dd[ictr+1,2]>dd[ictr,2]:
        start=ictr
        stop=ictr+1
        while stop<len(tt)-1 and dd[stop+1,6]>dd[stop,6]:# and dd[stop+1,2]>dd[stop,2]:
          stop+=1
        vrel=np.sqrt((vcart[start,0]-vcart[stop,0])**2+(vcart[start,1]-vcart[stop,1])**2)
        vrelcyl = [np.cos(dd[start,1])*(vcart[start,0]-vcart[stop,0])+np.sin(dd[start,1])*(vcart[start,1]-vcart[stop,1]),-np.sin(dd[start,1])*(vcart[start,0]-vcart[stop,0])+np.cos(dd[start,1])*(vcart[start,1]-vcart[stop,1])]
        jumps[i].append([start,stop,dd[stop,6]/dd[start,6],dd[stop,2]/dd[start,2],vrel/dd[start,5],np.arctan2(vrelcyl[0],vrelcyl[1]),tt[start],dd[stop,6]-dd[start,6],tt[stop]-tt[start]])
        ictr=stop
      else:
        ictr+=1
  #Infer how much un-interupted cooling time we've had
  tcool=[]
  jjs=[x for y in jumps for x in y]
  kcut=np.percentile([x[2] for x in jjs],k_perc_cut)
  scut=np.percentile([x[3] for x in jjs],s_perc_cut)
  for i in xrange(len(jumps)):
    tmp=[x for x in jumps[i] if x[2]>kcut and x[3]>scut and x[4]>1.0]
    #tmp=[x for x in jumps[i] if x[2]>1.15]
    if len(tmp)>1:
      for j in xrange(len(tmp)-1):
        tcool.append(tmp[j+1][6]-(tmp[j][8]+tmp[j][6]))
  np.savez('tcool.npz',tcool=tcool)
  #Heating time with different cutoffs
  htimes_max=[]
  for i in xrange(len(rdata)):
    print 'Extracting jumps in particle %d'%i
    dd=np.array(rdata[i][-1])
    tt=np.cumsum(1./dd[:,0]**1.5)
    vcart=np.c_[np.cos(dd[:,1])*dd[:,3]-np.sin(dd[:,1])*dd[:,4],np.sin(dd[:,1])*dd[:,3]+np.cos(dd[:,1])*dd[:,4]]
    #Find the heating points
    gain=0.0
    hstart=0
    heating=False
    heat=np.mean(dd[:,6])
    #heat=1e-10
    for ictr in xrange(len(tt)-1):
      if heating:
        if dd[ictr+1,6]>dd[ictr,6]:
          continue
        heating=False
        hstart=ictr+1
      gain += max(0,dd[ictr+1,6]-dd[ictr,6])
      if gain>=heat:
        htimes_max.append(tt[ictr+1]-tt[hstart])
        gain=0
        heating=True
        hstart=ictr+1
  htimes_min=[]
  for i in xrange(len(rdata)):
    print 'Extracting jumps in particle %d'%i
    dd=np.array(rdata[i][-1])
    tt=np.cumsum(1./dd[:,0]**1.5)
    vcart=np.c_[np.cos(dd[:,1])*dd[:,3]-np.sin(dd[:,1])*dd[:,4],np.sin(dd[:,1])*dd[:,3]+np.cos(dd[:,1])*dd[:,4]]
    #Find the heating points
    gain=0.0
    hstart=0
    heating=False
    for ictr in xrange(len(tt)-1):
      if heating:
        if dd[ictr+1,6]>dd[ictr,6]:
          continue
        heating=False
        hstart=ictr+1
      gain += max(0,dd[ictr+1,6]-dd[ictr,6])
      if gain>0:
        htimes_min.append(tt[ictr+1]-tt[hstart])
        gain=0
        heating=True
        hstart=ictr+1
  big_data.append([htimes_min,htimes_max])

#Create tcool for different track-lengths
for tgt in ['2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_3.0_N_25e4','2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_3.0_N_1e6','2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_3.0_N_4e6','2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_10.0_N_1e6']:
  os.chdir(base)
  os.chdir(tgt)
  #Load the tracking data
  print 'Loading pickled data for '+tgt
  with open('data.pickle','rb') as f:
    data=cPickle.load(f)
  #Now restrict to the different cut-offs, calculate and save
  for tcut in cutoffs:
    #Check that we'll have a reasonable number to track
    lls=np.array([len(x[-1]) for x in data])
    if np.sum(lls>tcut)<1000:
      print 'Only have %d particles tracked for %d or more snapshots. Skipping'%(np.sum(lls>tcut),tcut)
      continue
    print 'Calculating cutoffs with tracking for %d snapshots'%tcut
    rdata=[x[:-1]+[x[-1][-tcut:]] for x in data if len(x[-1])>tcut]
    #Get all the jumps
    jumps=[[] for i in xrange(len(rdata))]
    for i in xrange(len(rdata)):
      print i
      #Find all the jumps in entropy
      #0=R,1=phi,2=rho,3=vR,4=vphi,5=cs,6=K
      dd=np.array(rdata[i][-1])
      tt=np.cumsum(1./dd[:,0]**1.5)
      vcart=np.c_[np.cos(dd[:,1])*dd[:,3]-np.sin(dd[:,1])*dd[:,4],np.sin(dd[:,1])*dd[:,3]+np.cos(dd[:,1])*dd[:,4]]
      #Find the locations of shocks
      ictr=0
      while ictr<len(tt)-1:
        if dd[ictr+1,6]>dd[ictr,6]:# and dd[ictr+1,2]>dd[ictr,2]:
          start=ictr
          stop=ictr+1
          while stop<len(tt)-1 and dd[stop+1,6]>dd[stop,6]:# and dd[stop+1,2]>dd[stop,2]:
            stop+=1
          vrel=np.sqrt((vcart[start,0]-vcart[stop,0])**2+(vcart[start,1]-vcart[stop,1])**2)
          vrelcyl = [np.cos(dd[start,1])*(vcart[start,0]-vcart[stop,0])+np.sin(dd[start,1])*(vcart[start,1]-vcart[stop,1]),-np.sin(dd[start,1])*(vcart[start,0]-vcart[stop,0])+np.cos(dd[start,1])*(vcart[start,1]-vcart[stop,1])]
          jumps[i].append([start,stop,dd[stop,6]/dd[start,6],dd[stop,2]/dd[start,2],vrel/dd[start,5],np.arctan2(vrelcyl[0],vrelcyl[1]),tt[start],dd[stop,6]-dd[start,6],tt[stop]-tt[start]])
          ictr=stop
        else:
          ictr+=1
    #Infer how much un-interupted cooling time we've had
    tcool=[]
    jjs=[x for y in jumps for x in y]
    kcut=np.percentile([x[2] for x in jjs],k_perc_cut)
    scut=np.percentile([x[3] for x in jjs],s_perc_cut)
    for i in xrange(len(jumps)):
      tmp=[x for x in jumps[i] if x[2]>kcut and x[3]>scut and x[4]>1.0]
      #tmp=[x for x in jumps[i] if x[2]>1.15]
      if len(tmp)>1:
        for j in xrange(len(tmp)-1):
          tcool.append(tmp[j+1][6]-(tmp[j][8]+tmp[j][6]))
    np.savez('tcool_%d.npz'%tcut,tcool=tcool)


#Make plots at different run-lengths
#Test and compare run-size
#Pick any directory where we've calculated different track-length tcool objects
fig_a=plt.figure()
ax_a=fig_a.add_subplot(111)
fig_b=plt.figure()
ax_b=fig_b.add_subplot(111)
fig_c=plt.figure()
ax_c=fig_c.add_subplot(111)
cgen=ncolour()
for tgt in ['2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_3.0_N_25e4','2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_3.0_N_1e6','2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_3.0_N_4e6','2D_CD_alphaSPH_5.0_artCond_1.0_noSettle_beta_10.0_N_1e6']:
  os.chdir(base)
  os.chdir(tgt)
  c=cgen.next()
  lgen=nlstyle()
  #Do the actual plotting
  ##64-snapshots
  #lsty=lgen.next()
  #try:
  #  tc_64=np.load('tcool_64.npz')['tcool']
  #  x_64,y_64 = plotpdf(tc_64,ax=ax_a,color=c,linestyle=lsty)
  #  dx_64=np.diff(x_64[:2])[0]
  #  c_64=np.cumsum(y_64[::-1])[::-1]*dx_64
  #  ax_b.plot(x_64,c_64,color=c,linestyle=lsty)
  #  ax_c.plot(x_64,np.log10(c_64),color=c,linestyle=lsty)
  #except IOError:
  #  print 'Skipping 64 snapshot run for lack of data.'
  #128-snapshots
  lsty=lgen.next()
  try:
    tc_128=np.load('tcool_128.npz')['tcool']
    x_128,y_128 = plotpdf(tc_128,ax=ax_a,color=c,linestyle=lsty,linewidth=3)
    dx_128=np.diff(x_128[:2])[0]
    c_128=np.cumsum(y_128[::-1])[::-1]*dx_128
    ax_b.plot(x_128,c_128,color=c,linestyle=lsty,linewidth=3)
    ax_c.plot(x_128,np.log10(c_128),color=c,linestyle=lsty,linewidth=3)
  except IOError:
    print 'Skipping 128 snapshot run for lack of data.'
  #256-snapshots
  lsty=lgen.next()
  try:
    tc_256=np.load('tcool_256.npz')['tcool']
    x_256,y_256 = plotpdf(tc_256,ax=ax_a,color=c,linestyle=lsty,linewidth=3)
    dx_256=np.diff(x_256[:2])[0]
    c_256=np.cumsum(y_256[::-1])[::-1]*dx_256
    ax_b.plot(x_256,c_256,color=c,linestyle=lsty,linewidth=3)
    ax_c.plot(x_256,np.log10(c_256),color=c,linestyle=lsty,linewidth=3)
  except IOError:
    print 'Skipping 256 snapshot run for lack of data.'
  #512-snapshots
  lsty=lgen.next()
  try:
    tc_512=np.load('tcool_512.npz')['tcool']
    x_512,y_512 = plotpdf(tc_512,ax=ax_a,color=c,linestyle=lsty,linewidth=3)
    dx_512=np.diff(x_512[:2])[0]
    c_512=np.cumsum(y_512[::-1])[::-1]*dx_512
    ax_b.plot(x_512,c_512,color=c,linestyle=lsty,linewidth=3)
    ax_c.plot(x_512,np.log10(c_512),color=c,linestyle=lsty,linewidth=3)
  except IOError:
    print 'Skipping 512 snapshot run for lack of data.'
  #1024-snapshots
  lsty=lgen.next()
  try:
    tc_1024=np.load('tcool_1024.npz')['tcool']
    x_1024,y_1024 = plotpdf(tc_1024,ax=ax_a,color=c,linestyle=lsty,linewidth=3)
    dx_1024=np.diff(x_1024[:2])[0]
    c_1024=np.cumsum(y_1024[::-1])[::-1]*dx_1024
    ax_b.plot(x_1024,c_1024,color=c,linestyle=lsty,linewidth=3)
    ax_c.plot(x_1024,np.log10(c_1024),color=c,linestyle=lsty,linewidth=3)
  except IOError:
    print 'Skipping 1024 snapshot run for lack of data.'
  #Add labels as appropriate
  m=re.search('beta_([0-9]+)\.0_N_([0-9e]+)',tgt)
  ax_a.plot(-1,-1,color=c,label='$\\beta='+m.group(1)+',N='+m.group(2)+'$')
  ax_b.plot(-1,-1,color=c,label='$\\beta='+m.group(1)+',N='+m.group(2)+'$')
  ax_c.plot(-1,-1,color=c,label='$\\beta='+m.group(1)+',N='+m.group(2)+'$')

#Finish off plots
ax_a.set_xlabel('Time between shocks ($t_{dyn}$)')
ax_a.set_ylabel('Probability')
ax_a.set_title('Probability of different wait times between shock front passages')
ax_a.legend()
ax_a.set_xlim((0,50))
ax_a.set_ylim((0,.25))
fig_a.tight_layout()
ax_b.set_xlabel('Time between shocks ($t_{dyn}$)')
ax_b.set_ylabel('$P(t_{spi}>t)$')
ax_b.set_title('Probability of remaining unshocked for $t$ dynamical times or more')
ax_b.legend()
ax_b.set_xlim((0,50))
fig_b.tight_layout()
ax_c.set_xlabel('Time between shocks ($t_{dyn}$)')
ax_c.set_ylabel('$\\log_{10}(P(t_{spi}>t))$')
ax_c.set_title('Likelihood of remaining unshocked for $t$ dynamical times or more')
ax_c.legend()
ax_c.set_xlim((0,50))
ax_c.set_ylim((-6,0))
fig_c.tight_layout()


#Figure 3, the representative disc
#Calculate constant of proportionality for beta ~ R^9/2
#Assuming that beta=10 when R=50
prop_beta = 10/(50**(-9/2.))
#Now calculate the cut-off in R where number of clumps saturates
#Assume saturation at beta=5
#R_saturate = (5/prop_beta)**(-2/9.)
nmax=np.inf
#Now calculate the track
R=np.linspace(1,100,10000)
t1=(R**-1.5)*np.exp(-0.2*prop_beta*R**(-4.5))
n_clump=R**4.5/prop_beta
n_clump[n_clump>nmax]=nmax
t1=t1*n_clump
#A=0.1 is derived from log_10 plot, so to get the true value of A have to divide by log_10(e) giving 0.5 ish
#Normalise the probability per unit time so that P=2/3400 when beta=9.  This gives P=.03/beta np.exp(-0.5*beta).  We set beta=10*(50/R)**4.5
beta=20*(50./R)**4.5
#This is the probability per dynamical time, normalised
dp_tcut=(9.0/beta)*np.exp(-0.2*(beta-9.0))*(1.0/1700.0)
dp_dencut=(9.0/beta)*np.exp(-0.2*(beta-9.0)*np.log(1.0+20.*np.sqrt(20./beta))*(1./3))*(1.0/1700.0)
#To get from this to total probability, need to integrate over many little times dt, keeping things in units of dynamical times
#dp*time_period_in_dynamical_times
t1=dp_tcut*1e5/(2.*np.pi/R**1.5)
t2=dp_dencut*1e5/(2.*np.pi/R**1.5)
#t1=(0.03203/beta)*np.exp(-0.5*beta)
#t1=(0.03203/beta)*np.exp(-0.5*beta*np.log(1.+(1000./beta))*(1./6))
#Finally, the total probability is 1.0 minus the probability of it never fragmenting...
#Plot the result
plt.figure()
plt.plot(R,1.0-np.exp(-t1))
plt.plot(R,1.0-np.exp(-t2))
plt.xlabel("Radius (A.U.)")
plt.ylabel("Likelihood of fragmentation")
plt.title("Probability of fragmentation in the self-gravitating lifetime of a representative disc")


